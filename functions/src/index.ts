import * as functions from 'firebase-functions';
import admin = require('firebase-admin');

admin.initializeApp();
const idUserCintia = "XXn46sRNRzSySFFX2P79SP6pVtO2";

export const OnNewPedido = functions.database.ref('/Pedido/{data}').onCreate(async (snapshot: any, context: any) => {
    const pedido = snapshot.val();
    await admin.database().ref('/Tokens/' + idUserCintia).once('value').then(tokenRecord => {
        const payload: admin.messaging.MessagingPayload = {
                notification: {
                    title: 'Nuevo pedido recibido!',
                    body: 'Hay un nuevo pedido de ' + pedido.nombreCliente + ' en ' + pedido.nombreEstablecimiento + ' para  ser entregado en ' + pedido.lugarEntrega + ' - ' + pedido.direccionEntrega + ' telefono ' + pedido.telefonoCliente,
                    // icon: 'your-icon-url',
                    // click_action: 'FLUTTER_NOTIFICATION_CLICK' // ** do not put it here
                },
        };
        const options = {
            priority: "high",
            timeToLive: 50000,
        };
        setTimeout(() => admin.messaging().sendToDevice(tokenRecord.val().token, payload, options), 5000)

    })
});

export const OnOrdenTransporteupdate = functions.database.ref('/ordtransporte/{data}').onUpdate(async (snapshot: any, context: any) => {

    const orden = snapshot.after.val();
    console.log(orden)
    if (orden.idUserRepartidor !== 'computer') {
        await admin.database().ref('/Tokens/' + orden.idUserRepartidor).once('value').then(tokenRecord => {
            const payload: admin.messaging.MessagingPayload = {
                data: {
                    title: "NUEVA ENTREGA RECIBIDA",
                    body: "ESTABLECIMIENTO:" + orden.nombreEstablecimiento + "\nLUGAR DE ENTREGA: " + orden.lugarEntrega,
                },
            };

            const options = {
                priority: "high",
                timeToLive: 50000,
            };
            console.log('llegando')
            setTimeout(() => admin.messaging().sendToDevice(tokenRecord.val().token, payload, options), 5000)
        })
    }
});


exports.scheduledFunctionCrontab = functions.pubsub.schedule('every day 00:00')
    .timeZone('America/Lima')
    .onRun(async context => {

        await admin.database().ref('/Pedido').orderByChild('estado').equalTo('recibido').once('value').then(data => {
            const pedidos: any[] = [];
            data.forEach(child => {
                pedidos.push(child.val())
            });

            return Promise.all(pedidos.map(pedido => {
                pedido.estado = 'finalizado'
                pedido.idEst_Estado = pedido.idEstablecimiento + "_finalizado";
                return admin.database().ref('/Pedido/' + pedido.id).set(pedido).then(tokenRecord => {
                    console.log(tokenRecord);
                })
            }))

        });


        await admin.database().ref('/Pedido').orderByChild('estado').equalTo('PorRecoger').once('value').then(data => {
            var pedidos: any[] = [];
            data.forEach(child => {
                pedidos.push(child.val())
            });

            return Promise.all(pedidos.map(pedido => {
                pedido.estado = 'finalizado';
                pedido.idEst_Estado = pedido.idEstablecimiento + "_finalizado";
                return admin.database().ref('/Pedido/' + pedido.id).set(pedido).then(tokenRecord => {
                    console.log(tokenRecord);
                })
            }))

        });
    })


export const verificarPedidosNoAceptados = functions.pubsub.schedule('every 3 minutes').onRun(async context => {

    await admin.database().ref('/Pedido').orderByChild('estado').equalTo('porConfirmar').once('value').then(data => {
        const establecimientos: any[] = [];

        data.forEach(child => {
            establecimientos.push(child.val())
        });

        var no_duplicate = establecimientos.filter((objeto, index, self) => index === self.findIndex((t) => (t.idEstablecimiento === objeto.idEstablecimiento
        )));
        return Promise.all(no_duplicate.map(establecimiento => {
            return admin.database().ref('/Tokens/' + establecimiento.idEstablecimiento).once('value').then(tokenRecord => {
                const payload: admin.messaging.MessagingPayload = {

                    data: {
                        title: 'NUEVO PEDIDO RECIBIDO',
                        body: 'Por favor abra la aplicación',
                    },

                };

                var options = {
                    priority: "high",
                    timeToLive: 5000,
                };

                setTimeout(() => admin.messaging().sendToDevice(tokenRecord.val().token, payload, options), 5000)

            })
        }))

    });

})


// // NO BORRAR ESTO ES PARA QUE SE GENEREN DATOS PARA LOS REPORTES EN FIRESTORE

// export const detallePedidoForReport = functions.database.ref('/PedidoDetalle/{data}/').onCreate(async (snapshot: any, context: any) => {
//   var idPedido = context.params.data;

//   const pedidos = snapshot.val();

//   var a = admin.database().ref(`/Pedido/${idPedido}`).once('value').then(data => {

//     const pedido = data.val();

//     var infopedido = {
//       "nombreEstablecimiento": pedido.nombreEstablecimiento,
//       "idEstablecimiento": pedido.idEstablecimiento,
//     }

//     return infopedido;

//   });


//   await a.then(infopedido => {


//     Object.keys(pedidos).forEach(function (key) {
//       var detalleModificado = {
//         ...pedidos[key],
//         ...infopedido,
//         "idPedido": idPedido,
//         "fecha": new Date(),
//       }

//       admin.firestore().collection(`detallespedido`).add(detalleModificado).then(() => {
//         console.log('added order');
//       }, (error) => {
//         console.error('Failed to add order');
//         console.error(error);
//       });


//     })


//   })


// });

